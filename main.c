#include<stdio.h>
#include <sys/types.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

void script_print (pid_t* pid_ary, int size);

int main(int argc,char*argv[])
{
	if (argc != 2)
	{
		printf ("Wrong number of argument\n");
		exit (0);
	}

    int count = atoi(argv[1]);
 
    pid_t proID[count];
	memset(proID, 0, sizeof(pid_t) * count);
    pid_t cid;

    int stat;

    char *args[4];
    memset(args, '\n', sizeof(args[0][0] * 4 * 15));
    args[0] = "./iobound";
    args[1] = "-seconds";
    args[2] =  "10";
    args[3] = NULL;

    for (int i = 0; i < count; i++) {
        
        int pid = fork();
        *(proID + i) = pid;

        if (pid == 0) {
            execvp(args[0], args);

            perror("execvp Error");
            exit(1);
        }

        if (pid < 0) {
            perror("Fork Error");
        }

    }
    script_print(args, count);


    for (int i = 0; i < count; i++) {
        cid = waitpid(proID[i], &stat, 0);
        if (WIFEXITED(stat)) {
			// DO NOTHING
        }
    }


	for (int i = 0; i < count; i++) {
		kill(proID[i], SIGKILL);
	}

	// for (int i = 0; i < sizeof(args); i++) {
	// 	free(args[i]);
	// }
	//free(args);
	//free(proID);


	/*
	*	TODO
	*	#1	declear child process pool
	*	#2 	spawn n new processes
	*		first create the argument needed for the processes
	*		for example "./iobound -seconds 10"
	*	#3	call script_print
	*	#4	wait for children processes to finish
	*	#5	free any dynamic memories
	*/

	return 0;
}


void script_print (pid_t* pid_ary, int size)
{
	FILE* fout;
	fout = fopen ("top_script.sh", "w");
	fprintf(fout, "#!/bin/bash\ntop");
	for (int i = 0; i < size; i++)
	{
		fprintf(fout, " -p %d", (int)(pid_ary[i]));
	}
	fprintf(fout, "\n");
	fclose (fout);

	char* top_arg[] = {"gnome-terminal", "--", "bash", "top_script.sh", NULL};
	pid_t top_pid;

	top_pid = fork();
	{
		if (top_pid == 0)
		{
			if(execvp(top_arg[0], top_arg) == -1)
			{
				perror ("top command: ");
			}
			exit(0);
		}
	}
}


