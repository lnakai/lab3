all: iobound main

iobound: iobound.c
	$(CC) -o $@ $^

main: main.c
	$(CC) -o $@ $^

clean:
	rm iobound main text.txt top_script.sh