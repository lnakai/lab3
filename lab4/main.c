#include<stdio.h>
#include <sys/types.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
#include<signal.h>

void script_print (pid_t* pid_ary, int size);

void signaller(pid_t *pid_pool, int size, int signal) {
	for (int i = 0; i < size; i++) {
		sleep(2);
		printf("Parent process: %d - Sending signal: %d to child process: %d \n", getpid(), signal, pid_pool[i]);
		kill(pid_pool[i], signal);
	}
}

int main(int argc,char*argv[])
{
	if (argc != 2)
	{
		printf ("Wrong number of argument\n");
		exit (0);
	}

    int count = atoi(argv[1]);
 
    pid_t proID[count];
	memset(proID, 0, sizeof(pid_t) * count);
    pid_t cid;

	pid_t cID[count];
	memset(cID, 0, sizeof(pid_t) * count);

    int stat;

    char *args[4];
    memset(args, '\n', sizeof(args[0][0] * 4 * 15));
    args[0] = "./iobound";
    args[1] = "-seconds";
    args[2] =  "5";
    args[3] = NULL;

	sigset_t set;
	int sig;
	int *sigptr = &sig;
	int ret_val;

	int buf = 100;

	int child_pid;

	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigaddset(&set, SIGSTOP);
	sigaddset(&set, SIGCONT);
	sigaddset(&set, SIGINT);

	//sigprocmask(SIG_BLOCK, &set, NULL);
	sigprocmask(SIG_SETMASK, &set, NULL);

    for (int i = 0; i < count; i++) {
        
        int pid = fork();
        *(proID + i) = pid;

        if (pid == 0) {
			child_pid = getpid();
			cID[i] = child_pid;
			printf("Child Process: %d -Waiting for SIGUSR1...\n",child_pid);
			kill(child_pid, SIGUSR1);
			ret_val = sigwait(&set, sigptr);

			if (ret_val == 0) {
				printf("Child Process %d -Recieved signal: SIGUSR1 -Calling exec() \n", child_pid);
			} 
			if (ret_val == -1) {
				perror("sigwait failed\n");
			}

			

            execvp(args[0], args);

            perror("execvp Error");
            exit(1);
        } else {
			//signaller(proID, count, SIGUSR1);
			// signaller(proID, count, SIGSTOP);
			// wait(5);
			// signaller(proID, count, SIGCONT);
			// wait(3);
			// signaller(proID, count, SIGINT);
		}

        if (pid < 0) {
            perror("Fork Error");
        }

    }
	signaller(proID, count, SIGSTOP);
	sleep(5);
	signaller(proID, count, SIGCONT);
	sleep(3);
	signaller(proID, count, SIGINT);

    script_print(proID, count);


    for (int i = 0; i < count; i++) {
        cid = waitpid(proID[i], &stat, 0);
        if (WIFEXITED(stat)) {
			// DO NOTHING
        }
    }


	for (int i = 0; i < count; i++) {
		kill(proID[i], SIGKILL);
	}

	return 0;
}


void script_print (pid_t* pid_ary, int size)
{
	FILE* fout;
	fout = fopen ("top_script.sh", "w");
	fprintf(fout, "#!/bin/bash\ntop");
	for (int i = 0; i < size; i++)
	{
		fprintf(fout, " -p %d", (int)(pid_ary[i]));
	}
	fprintf(fout, "\n");
	fclose (fout);

	char* top_arg[] = {"gnome-terminal", "--", "bash", "top_script.sh", NULL};
	pid_t top_pid;

	top_pid = fork();
	{
		if (top_pid == 0)
		{
			if(execvp(top_arg[0], top_arg) == -1)
			{
				perror ("top command: ");
			}
			exit(0);
		}
	}
}


